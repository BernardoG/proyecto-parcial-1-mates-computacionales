#include <vector>
#include <iostream>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cstdio>
#include <iterator>
#include <queue>
#include <fstream>
#include <iostream>
#include <string>
#include <deque>
#include <algorithm>
#include <iterator>
#include <sstream>

//#define DEBUG 1

using namespace std;

inline int clamp(int n, int a, int b) {
    return min(max(n, a), b);
}
//
inline int get_suffix(string pq, string pk) {
    int j = pk.size();

    // cout << pq.size() << " T " << pk.size() << " | " << pq << " W " << pk << " | ";

    //Caso de epsilon
    if (pk.size() == 0) {
        return 0;
    }

    //Comprueba si pk es sufijo de pq
    for (int i = pq.size() - 1; i >= 0 && j > 0 && pq[i] == pk[j - 1]; i--, j--);

    //checa si se recorre pk, si no es sufijo es -1
    if (j > 0) {
        return -1;
    }
    //Regresa el tama�o de pk, sufijo
    return pk.size();
}
//Construye la funcion de transicion del automata [Caracter,Estado]
//Ancho tama�o alfabeto
vector< map<char, int> > compute_transition_function(string str, vector<char> alphabet) {
    int size = str.size();
    //cout << size ;
    int sizeAlphabet = alphabet.size();
    //Se construye tabla de transiciones
    vector< map<char, int> > transitions(str.size() + 1);

    // cout << size << " " << sizeAlphabet << endl;

#ifdef DEBUG
    //Titulos de tabla
    cout << "q|" << "\t" << "pq|" << "\t" << "pk|" << "\t" << "Symbol|" << "\t" << "k|" << "\tValue|" << endl;
    cout << "------------------------------------------------" << endl;
#endif // DEBUG
    //Tabla
    for (int i = 0; i <= size; i++) {
        for (int j = 0; j < sizeAlphabet; j++) {
            int k = min(size + 1, i + 2);
            // cout << k << endl;
            int suffix = 0;

            do {
                k = clamp(k - 1, 0, size);
                suffix = get_suffix(str.substr(0, i) + alphabet[j], str.substr(0, k));
#ifdef DEBUG
                // cout << suffix << endl;
                if(suffix == -1) {
                  cout << i << "\t" << (str.substr (0, i) + alphabet[j]) << "\t" << str.substr(0, k) << "\t" << alphabet[j] << "\t" << k << "\tNO" << endl;
                } else {
                   cout << i << "\t"<< (str.substr (0, i) + alphabet[j]) << "\t" << str.substr(0, k) << "\t" << alphabet[j] << "\t" << k << "\t" << suffix << endl;
                }
#endif // DEBUG
            } while (suffix == -1);

            transitions[i].insert(pair<char, int>(alphabet[j], k));
        }
    }

    return transitions;
} // compute_transition_function

//Cuenta las ocurrencias del match del patron con la cadena de prueba
int finite_automaton_match(string str, vector< map<char, int> > transitions, int m) {
    int n = str.size();
    int q = 0;
    int ocurrences = 0;

    //Se cuentan las ocurrencias cuando se llega al estado final (tama�o del patron)
    for (int i = 0; i < n; i++) {
        q = transitions[q][str[i]];
        if (q == m) {
            ocurrences++;
        }
    }
    return ocurrences;
}

int main() {
    string pattern, busqueda;

    //ifstream fin("datos.txt");
    /*
    fin >> pattern >> busqueda;
    */
    cin >> pattern;
    cin >> busqueda;

#ifdef DEBUG
    cout << "Patron: " << pattern << "\nTexto de busqueda: " << busqueda << "\n";
#endif // DEBUG

    //Se construye el alfabeto del patron
    vector<char> str_split(busqueda.begin(), busqueda.end());
    set<char> str_unique(str_split.begin(), str_split.end());
    vector<char> alphabet(str_unique.size());
    copy(str_unique.begin(), str_unique.end(), alphabet.begin());

    vector< map<char, int> > transitions = compute_transition_function(pattern, alphabet);

#ifdef DEBUG
    cout << "Ocurrencias: " << finite_automaton_match(busqueda, transitions, pattern.size()) << endl;
#else
    cout << finite_automaton_match(busqueda, transitions, pattern.size()) << endl;
#endif // DEBUG
    return 0;
} // main
