#include <iostream>
#include <cstring>
#include <algorithm>
#include <string>
#include <cassert>
#include <iomanip>
using namespace std;

inline int lchomsky(string a) {
    if (a.length() == 1 && a[0] >= 'A' && a[0] <= 'Z')
        return 1;
    return 0;
}

inline int rchomsky(string a) {
    if (a.length() == 1 && a[0] >= 'a' && a[0] <= 'z')
        return 1;
    if (a.length() == 2 && a[0] >= 'A' && a[0] <= 'Z' && a[1] >= 'A' && a[1] <= 'Z' )
        return 1;
    return 0;
}
int main(int argc, char const * argv[]) {
    string produccionInicial = "S";
    string gramatica[100][100] = {"S-AB|BC","A-BA|a","B-CC|b","C-AB|a"};
    string cadena;
    int numProducciones;

    std::cin >> numProducciones;
    std::cin >> produccionInicial;
/*
 *  //Introduce las producciones
 *  for (size_t i = 0; i < numProducciones; i++) {
 *      std::cin >> produccion;
 *      aux = produccion.find("->");
 *      //Obtengo loa produccion
 *      gramatica[i][0] = produccion.substr(0,aux);
 *      if (lchomsky(gram[i][0]) == 0){
 *          cout<<"\nLa gramatica no esta en forma de chomsky";
 *          break;
 *      }
 *  }
 */
    std::cout << "Introduce la cadena a evaluar: ";
    std::cin >> cadena;

    for (size_t i = 0; i < gramatica.length(); i++) {
        for (size_t j = 0; j < gramatica[i].length(); j++) {
            printf("Produccion: %s\n", gramatica[i][j]);
        }
    }
    return 0;
} // main
