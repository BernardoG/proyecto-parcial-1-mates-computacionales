gramatica = dict(
S = ['AB', 'BC'],
A = ['BA','a'],
B = ['CC', 'b'],
C = ['AB','a']
)

#Verifica que la gramatica este en FNC
def chomsky(gramatica):
    #list1 = []
    for rule in gramatica:
        for production in gramatica[rule]:
            if len(production)<=2:
                print("La gramatica esta en forma de chomsky")
                return 1
                #print(production)
            else:
                return 0
    #print(list1)
    #s = set(list1)
    return(0)

def encuentra(element):
    #BUSCA DENTRO DE LA GRAMATICA SI EL ELEMENTO ES PRODUCIDO Y REGRESA UN SET DE LAS PRODUCCIONES QUE LA HACE
    list1 = []
    for rule in gramatica:
        for production in gramatica[rule]:
            if production.count(element) == 1:
                list1.append(rule)
    #print(list1)
    s = set(list1)
    return(s)

def producciones(set1, set2):
    #REALIZA LAS CONCATENACIONES DE EVALUACION EN LA TABLA SI HAY MAS DE 1 PRODUCCION POR CASILLA
    aux = set ()
    for v1 in set1:
        for v2 in set2:
            aux = aux | encuentra(v1+v2)
    return aux


def cyk(cadena):
    #ITERA Y CONSTRUYE LA TABLA DE EVALUACION
    m = []
    a = []
    for c in cadena:
        a.append(encuentra(c))
    m.append(a)
    n = len(cadena)
    #recorrido verrtical
    for i in range(2, n + 1):
        m.append([])
        #Recorrido horizontal
        for j in range(1, n - i + 2):
            aux = set ()
            #print (m)
            #Recorrido en diagonal
            for k in range(1, i):
                #print("P(%d %d, A) = P(%d %d, B) + P(%d %d, C)" % (i - 1, j - 1, k - 1, j - 1, i - k - 1, j + k - 1))
                aux = aux | producciones(m[k - 1][j - 1], m[i - k - 1][j + k - 1])
            m[i - 1].append(aux)
        #print()
    #print (m)
    return m

def show_m(m, cadena):
    #IMPRIME LA TABLA CON FORMATO
    n = len(cadena)
    for i in range(n - 1, -1, -1):
        for j in m[i]:
            if(len(j) == 0):
                print("%10s " % ("0"), end='')
            else:
                print("%10s " % (','.join(j)), end='')
        print()
    print("-----------" * n)
    for i in cadena:
        print("%10s " % (i), end='')
    print()

def check(m, prod_inicial):
    #VERIFICA QUE LA CADENA SE ACEPTE CON LA PRIMER PRODUCCION, SI ESTA EL ESTADO INICIAL, SE ACENTA
    n = len(m)
    print("Produccion final: %s" % m[n-1][0])
    if prod_inicial in m[n-1][0]:
        print("Se acepta")
    else:
        print("Se rechaza")

#main

print()
inicial = 'S'
c = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
if chomsky(gramatica):
    res = cyk(c)
    print("Cadena: %s" % c)
    check(res,inicial)
    show_m(res, c)
else:
    print("La gramatica no esta en forma de chomsky")
print()


print()
c = "abababb"
if chomsky(gramatica):
    res = cyk(c)
    print("Cadena: %s" % c)
    check(res,inicial)
    show_m(res, c)
else:
    print("La gramatica no esta en forma de chomsky")
print()

