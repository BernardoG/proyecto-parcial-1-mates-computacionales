#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <math.h>
#include <assert.h>
using namespace std;

typedef long long unsigned int lli;

lli custom_pow(lli b, lli e, lli c) {
    lli ans = 1;

    lli n = b;

    while (e) {
        if (e & 1) {
            ans = (n * ans) % c;
        }
        n = (n * n) % c;

        e >>= 1;
    }
    return ans;
}

lli gcdExtended(lli a, lli b, lli * x, lli * y){
    if (a == 0) {
        *x = 0, *y = 1;
        return b;
    }

    lli x1, y1;
    lli gcd = gcdExtended(b % a, a, &x1, &y1);

    *x = y1 - (b / a) * x1;
    *y = x1;

    return gcd;
}

lli gcdNormal(lli a, lli b){
    return b == 0 ? a : gcdNormal(b, a % b);
}

int mul_inv(int a, int b){
    int b0 = b, t, q;
    int x0 = 0, x1 = 1;

    if (b == 1) return 1;
    while (a > 1) {
        q = a / b;
        t = b, b = a % b, a = t;
        t = x0, x0 = x1 - q * x0, x1 = t;
    }
    if (x1 < 0) x1 += b0;
    return x1;
}

int giveCoprime(int phiN){
    // Encontramos el primer coprimo que aparezca
    int aux = 0;
    int resp = 0;

    for (int i = 2; i < phiN; i++) {
        // cout << i << " " << phiN << " " << gcdNormal(i, phiN) << endl;
        if (gcdNormal(i, phiN) == 1) {
            return i;
        }
    }
    return resp;
}

lli cipher(lli m, lli e, lli n){
    return custom_pow(m, e, n);
}

lli decipher(lli d, lli c, lli n){
    return custom_pow(c, d, n);
}

void keyGen(lli p, lli q, lli m){
    lli n = p * q;
    lli d = 0;
    lli e = 0;

    cout << "\n" << "n = " << n << "\n";
    lli phiN = (p - 1) * (q - 1);
    cout << "phi(N) = " << phiN << "\n";
    while (d == 0) {
        e = giveCoprime(phiN);
        d = mul_inv(e, phiN);
    }

    cout << "Llave publica e = " << e << "\n";
    cout << "Llave privada d = " << d << "\n";
    cout << "Mensaje = " << m << "\n";
    cout << "Mensaje encriptado = " << cipher(m, e, n) << "\n";
    cout << "Mensaje desencriptado = " << decipher(d, cipher(m, e, n), n) << "\n";
}

bool is_prime(lli n) {
    for (int i = 2; i * i <= n; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int main(int argc, const char * argv[]) {

    lli p, q, m;
    std::cout << "Introduce p: ";
    std::cin >> p;
    std::cout << "Introduce q: ";
    std::cin >> q;

    if (p == q) {
        std::cout << "Error p y q deben ser distintos" << '\n';
    } else {
        std::cout << "Introduce el numero a crifrar: ";
        std::cin >> m;
        keyGen(p, q, m);
        std::cout << "\nMiss ponganos 100 porfavor :)" << '\n';
    }


    return 0;
}
