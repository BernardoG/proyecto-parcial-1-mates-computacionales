#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <math.h>
using namespace std;

// Source for modInverse: http://www.geeksforgeeks.org/multiplicative-inverse-under-modulo-m/

// C function for extended Euclidean Algorithm
int gcdExtended(int a, int b, int * x, int * y){
    // Base Case
    if (a == 0) {
        *x = 0, *y = 1;
        return b;
    }

    int x1, y1; // To store results of recursive call
    int gcd = gcdExtended(b % a, a, &x1, &y1);

    // Update x and y using results of recursive
    // call
    *x = y1 - (b / a) * x1;
    *y = x1;

    return gcd;
}

// C function for extended Euclidean Algorithm
int gcdNormal(int a, int b){
    // Base Case
    if (a == 0) {
        return b;
    }
    int gcd = gcdNormal(b % a, a);

    return gcd;
}

// Function to find modulo inverse of a
int modInverse(int a, int m){
    int x, y, res = 0;
    int g = gcdExtended(a, m, &x, &y);

    if (g != 1) {
        // cout << "Inverse doesn't exist\n";
    } else {
        // m is added to handle negative x
        res = (x % m + m) % m;
        // cout << "Modular multiplicative inverse is " << res << "\n";
    }
    return res;
}
/*
 * int coprime[46] = { 2,   3,   5,   7,   11,  13,  17,  19,  23,  29,  31,  37,  41,  43,  47,  53, 59, 61,
 *                  67,  71,  73,  79,  83,  89,  97,  101, 103, 107, 109, 113, 127, 131, 137,
 *                  139, 149, 151, 153, 163, 167, 173, 179, 181, 191, 193, 197, 199 };
 */
int giveCoprime(int phiN){
    int aux = 0;
    int resp = 0;

    while (resp == 0) {
        for (int i = 3; i < phiN; i += 2) {
            if (gcdNormal(i, phiN) == 1) {
                resp = i;
            }
            break;
        }
    }
    cout << "coprimo de " << phiN << ": " << resp << "\n";
    return resp;
}

int cipher(int m, int e, int n){
    double resp = pow(m, e);

    resp = fmod(resp, n);
    return resp;
}

int decipher(int d, int c, int n){
    double resp = pow(c, d);

    resp = fmod(resp, n);
    return resp;
}

void keyGen(int p, int q, int m){
    int n = p * q;
    int d = 0;
    int e;

    cout << "n = " << n << "\n";
    int phiN = (p - 1) * (q - 1);
    cout << "PhiN = " << phiN << "\n";
    while (d == 0) {
        e = giveCoprime(phiN);
        d = modInverse(e, phiN);
    }
    cout << "Llave publica e = " << e << "\n";
    cout << "Llave privada d = " << d << "\n";
    cout << "Mensaje = " << m << "\n";
    cout << "Mensaje encriptado = " << cipher(m, e, n) << "\n";
    cout << "Mensaje desencriptado = " << decipher(d, cipher(m, e, n), n) << "\n";
}

int main(int argc, const char * argv[]) {
    srand(time(0));

    keyGen(3, 11, 2);
    return 0;
}
